VFR Mini Plates
===============

What? Why?
----------

The VFR Mini Plates script generates a PDF document meant to be used on an ebook reader such as the Kobo or the Kindle. 

VFR mini plates are single pages displaying the important info on an airport:

- airport name and code
- altitude
- frequencies,
- circuit and integration altitude

as well as a diagram of the runways and the direction of the circuit.

![A picture is worth a thousand words.](https://bitbucket.org/ldom/vfr_mini_plates/raw/6484d38cd3dd09d6069811b0842ea50c0560379a/kobo.jpg)

**Note: mini plates aren't supposed to replace the official documentation. Use at your own risk and look outside!**

Requirements
------------

Python 2.7.x

Packages listed in the requirements.txt file. To automatically install them, install pip then run:


    pip install -r requirements.txt

Poppler (for merging the generated pdf files into one): [poppler.freedesktop.org](http://poppler.freedesktop.org)


    brew install poppler

Usage:
------

    python mini_plates.py <json data>

Source data
-----------

The input to provide to the script is a file containing [JSON](http://json.org) data. 

It contains an array of airports. The example below shows 2 airports: one with a single runway and one with 2 intersecting runways (these are the currently supported layouts).

Notes: 

- The script will display properly up to 3 lines from the `frequencies` array.
- The `surface` property controls the color of the runway on the diagram (`paved` = black, `unpaved` = grey)


```json
    {
        "airports" : [
    
            {
                "name": "Dinard", "code": "LFRD", "elevation": "219 ft",
                "frequencies": [
                    { "code": "TWR", "frequency": "120.15" },
                    { "code": "ATIS", "frequency": "124.575" },
                    { "code": "VOR", "frequency": "114.3" }
                ],
                "circuit_altitude": "1200", "integration_altitude": "1700",
                "runway_layout": "two_intersecting",
                "preferred": "35",
                "runways": [
                        { 
                            "name": "17-35", "intersection": "middle", 
                            "surface": "paved",
                            "orientations": [
                                { "name": "17", "qfu": 172, "length": 2200, "width": 45, "circuit": "right"  },
                                { "name": "35", "qfu": 352, "length": 2200, "width": 45, "circuit": "left"  }
                            ]
    
                        },
                        {
                            "name": "12-30", "intersection": "middle", 
                            "surface": "paved",
                            "orientations": [
                                { "name": "12", "qfu": 117, "length": 1445, "width": 45, "circuit": "left"  },
                                { "name": "30", "qfu": 297, "length": 1445, "width": 45, "circuit": "right"  }
                            ]
                        }
                    ]
            },
            {
                "name": "Dinan", "code": "LFEB", "elevation": "393 ft",
                "frequencies": [
                    { "code": "TWR", "frequency": "120.15" },
                    { "code": "APP", "frequency": "126.95" },
                    { "code": "A/A", "frequency": "123.5" }
                ],
                "circuit_altitude": "1400", "integration_altitude": "1900",
                "runway_layout": "single",
                "preferred": "",
                "runways": [
                        { 
                            "name": "07-25", 
                            "surface": "paved",
                            "orientations": [
                                { "name": "07", "qfu": 73, "length": 830, "width": 30, "circuit": "left"  },
                                { "name": "25", "qfu": 253, "length": 830, "width": 30, "circuit": "left"  }
                            ]
    
                        }
                    ]
            }
        ]
    }
```

License
-------

[GPL v3 or later](https://www.gnu.org/licenses/gpl.txt)


TODO and next steps
-------------------

Next things I plan to do.

- proportionally display of the runway length
- accurate location of the intersection
- non-intersecting runways
- layouts for more than 2 runways
- cleaner management of temporary files
- a nice web site to generate these from a web form with community-maintained data

