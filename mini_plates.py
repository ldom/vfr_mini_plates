#!/usr/bin/env python
# -*- coding: utf-8 -*-

########################################################################################################################
#
# VFR mini plates generator 
# Copyright (c) 2014 Laurent Domenech-Cabaud.
#
# Distributed under the GNU GPL v3 or later. For full terms see https://www.gnu.org/licenses/gpl.txt
#
########################################################################################################################

import argparse
import logging
import simplejson as json
import math
import subprocess

########################################################################################################################

import svgwrite
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPDF

########################################################################################################################

def draw_airport_info(dwg, info):
    margin_left = 10
    margin_top = 50
    large_text_size = '50'
    large_vertical_offset = 55
    medium_text_size = '25'
    medium_vertical_offset = 35
    
    x = margin_left
    y = margin_top

    dwg.add(dwg.text(info['name'] + ' - ' + info['code'], insert=(x, y), fill='black', font_size=large_text_size, font_weight='bold'))

    # frequencies    
    y += large_vertical_offset + 10
    y_base = y
    widest_string = 0
    for freq in info['frequencies']:
        new_text = dwg.add(dwg.text(freq['code'][0:7], insert=(x, y), fill='black', font_size=large_text_size))
        if len(freq['code']) > widest_string:
            widest_string = len(freq['code'][0:7])
        y += large_vertical_offset

    y = y_base
    for freq in info['frequencies']:
        dwg.add(dwg.text(freq['frequency'], insert=(x + (widest_string * 40), y), fill='black', font_size=large_text_size, font_weight='bold'))
        y += large_vertical_offset

    # altitudes
    y = margin_top
    dwg.add(dwg.text(info['elevation'], insert=(600 - margin_left, y), fill='black', font_size=large_text_size, text_anchor='end'))
    y += medium_vertical_offset + 10
    dwg.add(dwg.text('INTEGRATION', insert=(600 - margin_left, y), fill='black', font_size=medium_text_size, text_anchor='end'))
    y += large_vertical_offset - 8
    dwg.add(dwg.text(info['integration_altitude'], insert=(600 - margin_left, y), fill='black', font_size=large_text_size, text_anchor='end', font_weight='bold'))
    y += medium_vertical_offset
    dwg.add(dwg.text('CIRCUIT', insert=(600 - margin_left, y), fill='black', font_size=medium_text_size, text_anchor='end'))
    y += large_vertical_offset - 8
    dwg.add(dwg.text(info['circuit_altitude'], insert=(600 - margin_left, y), fill='black', font_size=large_text_size, text_anchor='end', font_weight='bold'))

    return True

def point_at_angle((start_x, start_y), angle, distance):
    x = (math.sin(math.radians(180-angle)) * distance) + start_x
    y = (math.cos(math.radians(180-angle)) * distance) + start_y     
    return (x, y)
    
def draw_left_arrow_sq(dwg, orientation, dest_x, dest_y):
    # arrow
    (p1_x, p1_y) = point_at_angle((dest_x, dest_y), orientation-150, 30)
    (p2_x, p2_y) = point_at_angle((dest_x, dest_y), orientation+150, 30)

    dwg.add(dwg.polygon([(dest_x, dest_y), (p1_x, p1_y), (p2_x, p2_y)], stroke_width=1, stroke='black'))

    (arrow_base_x, arrow_base_y) = point_at_angle((dest_x, dest_y), orientation-180, 20)
    (handle_x, handle_y) = point_at_angle((dest_x, dest_y), orientation-180, 80)


    dwg.add(dwg.line((handle_x, handle_y), (arrow_base_x, arrow_base_y), stroke_width=8, stroke='black'))

    (source_x, source_y) = point_at_angle((handle_x, handle_y), orientation-90, 60)
    dwg.add(dwg.line((handle_x, handle_y), (source_x, source_y), stroke_width=8, stroke='black'))

def draw_left_arrow(dwg, orientation, dest_x, dest_y):
    # arrow
    (p1_x, p1_y) = point_at_angle((dest_x, dest_y), orientation-150, 30)
    (p2_x, p2_y) = point_at_angle((dest_x, dest_y), orientation+150, 30)

    dwg.add(dwg.polygon([(dest_x, dest_y), (p1_x, p1_y), (p2_x, p2_y)], stroke_width=8, stroke='black'))

    (arrow_base_x, arrow_base_y) = point_at_angle((dest_x, dest_y), orientation-180, 28)
    (handle_x, handle_y) = point_at_angle((dest_x, dest_y), orientation-180, 80)
    (source_x, source_y) = point_at_angle((handle_x, handle_y), orientation-90, 60)

    path = dwg.add(dwg.path(d=("M%d,%d Q%d,%d %d,%d" % (arrow_base_x, arrow_base_y, handle_x, handle_y, source_x, source_y)), stroke_width=8, stroke='black', fill='none'))
    
def draw_right_arrow(dwg, orientation, dest_x, dest_y):
    # arrow
    (p1_x, p1_y) = point_at_angle((dest_x, dest_y), orientation-150, 30)
    (p2_x, p2_y) = point_at_angle((dest_x, dest_y), orientation+150, 30)

    dwg.add(dwg.polygon([(dest_x, dest_y), (p1_x, p1_y), (p2_x, p2_y)], stroke_width=8, stroke='black'))

    (arrow_base_x, arrow_base_y) = point_at_angle((dest_x, dest_y), orientation-180, 28)
    (handle_x, handle_y) = point_at_angle((dest_x, dest_y), orientation-180, 80)
    (source_x, source_y) = point_at_angle((handle_x, handle_y), orientation+90, 60)

    path = dwg.add(dwg.path(d=("M%d,%d Q%d,%d %d,%d" % (arrow_base_x, arrow_base_y, handle_x, handle_y, source_x, source_y)), stroke_width=8, stroke='black', fill='none'))
    
def draw_centered_runway_line(dwg, runway1, runway2, surface = 'paved', preferred = False):
    center_x = 300
    center_y = 512

    text_pos_offset = 40
    text_pos_y_offset = 15

    turn_pos_offset = 40
    
    length = 220
    
    (x, y) = point_at_angle((center_x, center_y), runway1['qfu']+180, length/2)

    text1_x = (math.sin(math.radians(360-runway1['qfu'])) * ((length/2)+text_pos_offset)) + center_x
    text1_y = (math.cos(math.radians(360-runway1['qfu'])) * ((length/2)+text_pos_offset)) + center_y + text_pos_y_offset

    (turn1_x, turn1_y) = point_at_angle((center_x, center_y), runway1['qfu']+180, (length/2)+text_pos_offset+turn_pos_offset)
    
    (x2, y2) = point_at_angle((center_x, center_y), runway1['qfu'], length/2)

    text2_x = (math.sin(math.radians(180-runway1['qfu'])) * ((length/2)+text_pos_offset)) + center_x
    text2_y = (math.cos(math.radians(180-runway1['qfu'])) * ((length/2)+text_pos_offset)) + center_y + text_pos_y_offset

    (turn2_x, turn2_y) = point_at_angle((center_x, center_y), runway1['qfu'], (length/2)+text_pos_offset+turn_pos_offset)

    if surface == 'paved':
        dwg.add(dwg.line((x, y), (x2, y2), stroke_width=30, stroke='black'))
    else:
        dwg.add(dwg.line((x, y), (x2, y2), stroke_width=30, stroke='#aaa'))
    
    dwg.add(dwg.text(runway1['name'], insert=(text1_x, text1_y), font_size='50', text_anchor='middle', font_weight='bold'))
    if (runway1['name'] == preferred):
        dwg.add(dwg.circle(center=(text1_x+1, text1_y-text_pos_y_offset-3), r = 33, stroke_width=2, stroke='black', fill='none'))
        
    dwg.add(dwg.text(runway2['name'], insert=(text2_x, text2_y), font_size='50', text_anchor='middle', font_weight='bold'))
    if (runway2['name'] == preferred):
        dwg.add(dwg.circle(center=(text2_x+1, text2_y-text_pos_y_offset-3), r = 33, stroke_width=2, stroke='black', fill='none'))

    if runway1['circuit'] == 'left':
        draw_left_arrow(dwg, runway1['qfu'], turn1_x, turn1_y)
    else:
        draw_right_arrow(dwg, runway1['qfu'], turn1_x, turn1_y)

    if runway2['circuit'] == 'left':
        draw_left_arrow(dwg, runway2['qfu'], turn2_x, turn2_y)
    else:
        draw_right_arrow(dwg, runway2['qfu'], turn2_x, turn2_y)

def draw_single_runway(dwg, rwy_info, preferred):
    rwy = rwy_info[0]
    draw_centered_runway_line(dwg, rwy['orientations'][0], rwy['orientations'][1], rwy['surface'], preferred)

        
def draw_two_intersecting_runways(dwg, rwy_info, preferred):
    rwy1 = rwy_info[0]
    draw_centered_runway_line(dwg, rwy1['orientations'][0], rwy1['orientations'][1], rwy1['surface'], preferred)
    rwy2 = rwy_info[1]
    draw_centered_runway_line(dwg, rwy2['orientations'][0], rwy2['orientations'][1], rwy2['surface'], preferred)



if __name__ == "__main__":
    # CLI 
    parser = argparse.ArgumentParser(description='VFR mini plates generator, v0.2. (c) Ldom, 2014. All rights reserved.')
    parser.add_argument('airport_data_file', help='File containing airport data in JSON format')
    args  = parser.parse_args()
    settings = {'data': args.airport_data_file }
    
    try:
        f = open(args.airport_data_file)
        json_data = f.read()
    finally:
        f.close()
        
    airport_data = json.loads(json_data)
    
    pdf_merge_commandline = "pdfunite "
    
    for airport in airport_data['airports']:
        dwg = svgwrite.Drawing(airport['code']+'.svg', profile='full', size=('600', '800'))
        draw_airport_info(dwg, airport)
        if airport['runway_layout'] == 'single':
            draw_single_runway(dwg, airport['runways'], airport['preferred'])
        elif airport['runway_layout'] == 'two_intersecting':
            draw_two_intersecting_runways(dwg, airport['runways'], airport['preferred'])
        dwg.save()
            
        drawing = svg2rlg(airport['code']+'.svg')
        renderPDF.drawToFile(drawing, airport['code']+'.pdf')
        
        pdf_merge_commandline += airport['code'] + '.pdf' + " "
        
    pdf_merge_commandline += "complete.pdf"
    
#    print pdf_merge_commandline
    subprocess.call(pdf_merge_commandline, shell=True)

